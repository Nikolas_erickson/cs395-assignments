/*********************************************
 * Id: eric7862
 *
 * performs a selection sort
 *
 * usage example:
 * w09 5628 6332 712 1585 7401 4430 6444 5153 5000 4940 3087 9973 54 523 2328 6907 7358 5769 6402 7964 4373 3735 207 8696 2343 6935 8770 8630 2246 5073 480
 * returns
 * 54 207 523 712 1585 2246 2328 2343 3087 3735 4373 4430 4803 4940 5000 5073 5153 5628 5769 6332 6402 6444 6907 6935 7358 7401 7964 8630 8696 8770 9973 
 *
 *********************************************/
#include <stdio.h>
#include <stdlib.h>

void SS(int A[], int n);
void printArray(int A[], int n);

int main(int argc, char *argv[])
{
   if (argc < 2)
   {
      printf("Usage: %s int1 int2 .. intN\n", argv[0]);
      printf("sorts an array of integers.\n");
      return 0;
   }

   int n = argc - 1;
   int A[n];
   for (int i = 0; i < n; i++)
   {
      A[i] = atoi(argv[i + 1]);
   }
   SS(A, n);
   return 0;
}

/// @brief Performs a selection sort
/// @param A pointer to array
/// @param n length of array
void SS(int A[], int n)
{
   printArray(A, n);
   for (int i = 0; i < n - 1; i++)
   {
      int min = i;
      for (int j = i + 1; j < n; j++)
      {
         if (A[j] < A[min])
         {
            min = j;
         }
      }
      int tmp = A[min];
      A[min] = A[i];
      A[i] = tmp;
      printArray(A, n);
   }
}

// prints the array
void printArray(int A[], int n)
{
   // Note: in printArray, print each value as an integer places followed by space:
   for (int i = 0; i < n; i++)
   {
      printf("%d ", A[i]);
   }
   printf("\n");
}