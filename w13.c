/*********************************************
 * Id: eric7862
 *
 * performs a russian peasant multiplication
 *
 * usage example:
 *
./w13 50 65
    50     65
    25    130    130
    12    260
     6    520
     3   1040   1040
     1   2080   2080
              ------
                3250
 *********************************************/
#include <stdio.h>
#include <stdlib.h>

void russianPeasantMultiplication(int n, int m);
void printLine(int n, int m);

int main(int argc, char ** argv)
{
   if(argc < 3)
   {
      printf("Usage: %s int1 int2\n", argv[0]);
      printf("Multiplies two ints using the Russian Peasant Multiplication method\n");
      return 0;
   }

   int n1 = atoi(argv[1]);
   int n2 = atoi(argv[2]);
   russianPeasantMultiplication(n1, n2);
   return 0;
}

/// @brief Does russian peasant multiplication
/// @param n multiplicand 1
/// @param m multiplicand 2
void russianPeasantMultiplication(int n, int m)
{
   int n1 = n;
   int n2 = m;
   int sum = 0;
   printLine(n, m);
   while(n1 > 1)
   {
      n1 /= 2;
      n2 *= 2;
      if(n1 % 2 == 1)
      {
         //if odd
         sum += n2;
      }
      printLine(n1, n2);
   }
   printf("              ------\n");
   printf("              %6d\n", sum);
}

/// @brief prints the appropriate line for either odd or even N
/// @param n multiplicand 1
/// @param m multiplicand 2
void printLine(int n, int m)
{
   if(n % 2 == 1)
   {
      //if odd
      printf("%6d %6d %6d\n", n, m, m);
   }
   else
   {
      //if even
      printf("%6d %6d\n", n, m);
   }
}