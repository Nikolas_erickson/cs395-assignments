/*********************************************
 * Id: eric7862
 *
 * performs an insertion sort
 *
 * usage example:
 * w11 89 68 90 29 34 17
 * returns
   89  | 45 68 90 29 34 17
   45 89  | 68 90 29 34 17
   45 68 89  | 90 29 34 17
   45 68 89 90  | 29 34 17
   29 45 68 89 90  | 34 17
   29 34 45 68 89 90  | 17
   17 29 34 45 68 89  90
 *********************************************/
#include <stdio.h>
#include <stdlib.h>

void InsertionSort(int *A, int size);
void swap(int *i1, int *i2);
void printArray(int *A, int size, int i);

int main(int argc, char *argv[])
{
   if (argc < 2)
   {
      printf("Usage: %s int1 int2 .. intN\n", argv[0]);
      printf("sorts an array of integers.\n");
      return 0;
   }

   int n = argc - 1;
   int A[n];
   for (int i = 0; i < n; i++)
   {
      A[i] = atoi(argv[i + 1]);
   }
   InsertionSort(&A[0], n);
   return 0;
}

/// @brief prints an array
/// @param A pointer to array
/// @param size size of array
/// @param i index of how much of the array is sorted so far
void printArray(int *A, int size, int i)
{
   // Note: in printArray, print each value as an integer places followed by space:
   for (int j = 0; j < size; j++)
   {
      printf("%d ", A[j]);
      if (i == j)
      {
         printf("| ");
      }
   }
   printf("\n");
}

// Sorts a given array by insertion sort
// Input: An array A[0..n − 1] of integers given as command line arguements
// Output: Array A[0..n − 1] sorted in nondecreasing order
void InsertionSort(int *A, int size)
{
   printArray(A, size, 0);
   for (int i = 1; i < size; i++)
   {
      int v = A[i];
      int j = i - 1;
      while (j >= 0 && A[j] > v)
      {
         A[j + 1] = A[j];
         j--;
      }
      A[j + 1] = v;
      printArray(A, size, i);
   }
}