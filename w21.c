// Nik Erickson
// Permutations




#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct Queue {
   int front, rear, size, capacity;
   int * array;
} Queue;

Queue * createQueue();
int isFull(Queue * queue);
int isEmpty(Queue * queue);
void enqueue(Queue * queue, int item);
int dequeue(Queue * queue);
int front(Queue * queue);
void printQueue(Queue * queue);


Queue * createQueue()
{
   Queue * queue = (Queue *) malloc(sizeof(Queue));
   queue->capacity = 16;
   queue->front = queue->size = 0;
   queue->rear = 15;
   queue->array = (int *) malloc(queue->capacity * sizeof(int));
   return queue;
}
int isFull(Queue * queue)
{
   return (queue->size == queue->capacity);
}
int isEmpty(Queue * queue)
{
   return (queue->size == 0);
}
void enqueue(Queue * queue, int item)
{
   if(isFull(queue))
   {
      int * newArray = (int *) malloc(queue->capacity * 2 * sizeof(int));
      for(int i = 0; i < queue->capacity; i++)
      {
         newArray[i] = queue->array[(queue->front + i) % queue->capacity];
      }
      free(queue->array);
      queue->array = newArray;
      queue->front = 0;
      queue->rear = queue->capacity - 1;
      queue->capacity *= 2;
      return;
   }
   queue->rear = (queue->rear + 1) % queue->capacity;
   queue->array[queue->rear] = item;
   queue->size = queue->size + 1;
}
int dequeue(Queue * queue)
{
   if(isEmpty(queue))
   {
      return -1;
   }
   int item = queue->array[queue->front];
   queue->front = (queue->front + 1) % queue->capacity;
   queue->size = queue->size - 1;
   return item;
}
int front(Queue * queue)
{
   if(isEmpty(queue))
   {
      return -1;
   }
   return queue->array[queue->front];
}
void printQueue(Queue * queue)
{
   int i = queue->front;
   while(i != queue->rear)
   {
      printf("%d ", queue->array[i]);
      i = (i + 1) % queue->capacity;
   }
   printf("%d\n", queue->array[i]);
}


void perms(const char* prefix, int* set, int setsize);


int main(int argc, char** argv)
{
	if(argc != 2)
	{
		printf("you need to specify exactly one argument, the number of items.");
		exit(1);
	}

	// get input as integer
	int in = atoi(argv[1]);

	// create set of items for permutations
	int* set = malloc(in * sizeof(int));
	// populate set
	for(int i=1; i<=in; i++)
	{
		set[i] = i+1;
	}

	// do the magic
	perms("", set, in);


}


/// create a list of permutations with a given prefix, "prefix", for the set "set" of size "setsize"
/// via recursion
void perms(const char* prefix, int* set, int setsize)
{
	// base case
	if(setsize == 1)
	{
		printf("%s%d\n", prefix, set[0]);
		return;
	}
	// create a queue
	Queue* q = createQueue();

	// populate queue with each element of the set
	for(int i=0; i<setsize; i++)
	{
		enqueue(q, set[i]);
	}


	// for each item in the queue
	while(!isEmpty(q))
	{
		// pop item
		int currentValue = dequeue(q);

		// create new set that is the same as the old set minus the popped item
		int* newSet = malloc((setsize-1)*sizeof(int));
		int itemSkipped = 0;
		for(int i=0; i<setsize-1; i++)
		{
			if(set[i] == currentValue)
			{
				itemSkipped++;
			}
			newSet[i] = set[i+itemSkipped];
		}

		// create new prefix
		char* newPrefix = malloc((strlen(prefix)+2)*sizeof(char));
		// by copying old prefix
		int i=0;
		for( ; i<strlen(prefix); i++)
		{
			newPrefix[i] = prefix[i];
		}
		// and appending current value
		newPrefix[i] = currentValue+48;
		newPrefix[i+1] = '\0';

		// recursively call this function with new data
		perms(newPrefix, newSet, setsize-1);

	}
}