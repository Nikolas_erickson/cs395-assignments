/*********************************************
 * Id: eric7862
 *
 * compares runtimes of different sorting algorithms
 * insertion sort, quicksort, and selection sort
 *
 * usage example:
 * w12 10
 * returns
  +-----------------------+-----------------+----------------+------------+
  |    Number of Elements |  Selection Sort | Insertion sort |  Quicksort |
  +-----------------------+-----------------+----------------+------------+
  |                     10|                0|               0|           0|
  |                    100|                0|               0|           0|
  |                   1000|                1|               2|           0|
  +-----------------------+-----------------+----------------+------------+
 *********************************************/
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>


void InsertionSort(int * A, int size);
void swap(int * i1, int * i2);
void Quicksort(int * A, int l, int r, int n);
int HoarePartition(int * A, int l, int r);
void SS(int A[], int n);
void printHeader();
void printFooter();
void testAndPrint(int n);

int main(int argc, char * argv[])
{
   srand(time(NULL));
   if(argc < 2)
   {
      printf("Usage: %s N\n", argv[0]);
      printf("where N is an integer.\n");
      printf("\n");
      printf("This program compares runtimes of three different sorting algorithms.\n");
      printf("It sorts arrays of size N, N*10, and then N*100 and then displays\n");
      printf("the results in a tabulated format.\n");
      return 0;
   }

   int n = atoi(argv[1]);
   //start the main program
   printHeader();
   int runNum = 1;
   while(runNum < 4)
   {
      testAndPrint(n);
      n *= 10;
      runNum += 1;
   }
   printFooter();

   return 0;
}

/// @brief prints header for table
void printHeader()
{
   printf("+-----------------------+-----------------+----------------+------------+\n");
   printf("|    Number of Elements |  Selection Sort | Insertion sort |  Quicksort |\n");
   printf("+-----------------------+-----------------+----------------+------------+\n");

}

/// @brief prints footer for table
void printFooter()
{
   printf("+-----------------------+-----------------+----------------+------------+\n");
}

/// @brief tests the sorts with array size n
/// @param n size of test array
void testAndPrint(int n)
{
   if(n > 1000000000) return;
   // generate randomized arrays
   int * a1 = malloc(sizeof(int) * n);
   int * a2 = malloc(sizeof(int) * n);
   int * a3 = malloc(sizeof(int) * n);
   int maxint = n * 100;
   for(int i = 0; i < n; i++)
   {
      int t = random() % maxint;
      a1[i] = a2[i] = a3[i] = t;
   }

   // test each sort and print results
   struct timeval start, stop;
   // run selection sort timing
   double selectionTime, insertionTime, quicksortTime;
   gettimeofday(&start, NULL);
   SS(a1, n);
   gettimeofday(&stop, NULL);
   selectionTime = (double) (stop.tv_sec - start.tv_sec) + (double) (stop.tv_usec - start.tv_usec) / 1000000;
   // run insertion sort timing
   gettimeofday(&start, NULL);
   InsertionSort(a2, n);
   gettimeofday(&stop, NULL);
   insertionTime = (double) (stop.tv_sec - start.tv_sec) + (double) (stop.tv_usec - start.tv_usec) / 1000000;
   // run quicksort timing
   gettimeofday(&start, NULL);
   Quicksort(a3, 0, n - 1, n);
   gettimeofday(&stop, NULL);
   quicksortTime = (double) (stop.tv_sec - start.tv_sec) + (double) (stop.tv_usec - start.tv_usec) / 1000000;
   free(a1);
   free(a2);
   free(a3);

   printf("|%23d|%17.f|%16.f|%12.f|\n", n, selectionTime * 1000, insertionTime * 1000, quicksortTime * 1000);
}

// Sorts a given array by insertion sort
// Input: An array A[0..n − 1] of integers given as command line arguements
// Output: Array A[0..n − 1] sorted in nondecreasing order
void InsertionSort(int * A, int size)
{
   for(int i = 1; i < size; i++)
   {
      int v = A[i];
      int j = i - 1;
      while(j >= 0 && A[j] > v)
      {
         A[j + 1] = A[j];
         j--;
      }
      A[j + 1] = v;
   }
}

// Sorts a given array by selection sort
// Input: Array of A[0,n-1]) of integers given as command line arguements
// Output: The integers sorted
void SS(int A[], int n)
{
   for(int i = 0; i < n - 1; i++)
   {
      int min = i;
      for(int j = i + 1; j < n; j++)
      {
         if(A[j] < A[min])
         {
            min = j;
         }
      }
      int tmp = A[min];
      A[min] = A[i];
      A[i] = tmp;
   }
}

// Sorts a subarray by quicksort
// Input: Subarray of array A[0..n − 1], of integers defined by its left and right
//  indices l and r
// Output: Subarray A[l..r] sorted in nondecreasing order
void Quicksort(int * A, int l, int r, int n)
{
   if(l < r)
   {
      int s = HoarePartition(A, l, r); // s is a split position
      if(s > 0)
      {
         Quicksort(A, l, s - 1, n);
      }
      if(s < n - 1)
      {
         Quicksort(A, s + 1, r, n);
      }
   }
}

// Partitions a subarray by Hoare’s algorithm, using the first element
//  as a pivot
// Input: Subarray of array A[0..n − 1], defined by its left and right
//  indices l and r (l<r)
// Output: Partition of A[l..r], with the split position returned as
//  this function’s value
int HoarePartition(int * A, int l, int r)
{
   // printArray(A, l, r, 6);
   int p = A[l];
   int i = l;
   int j = r + 1;
   while(1)
   {
      do
      {
         i++;
      }
      while(A[i] < p && i < r);
      do
      {
         j--;
      }
      while(A[j] > p);
      if(i >= j)
      {
         swap(&A[l], &A[j]);
         // printArray(A, l, r, 6);
         // printf("pivot A[%i]=%i\n", j, A[j]);
         return j;
      }
      swap(&A[i], &A[j]);
   }
}

/// @brief swaps elements of same type
/// @param i1 pointer to first item
/// @param i2 pointer to second item
void swap(int * i1, int * i2)
{
   // printf("swap %d, %d\n", *i1, *i2);
   int tmp = *i1;
   *i1 = *i2;
   *i2 = tmp;
}