/*********************************************
 * Id: eric7862
 * 
 * Tower of Hanoi solver
 * This program solves tower of hanoi problems of any size
 * It takes the number of disks in the problem as an integer perameter
 * 
 * usage example:
 * w05 2
 * returns
 * MOVE 1 2
 * MOVE 1 3
 * MOVE 2 3
 * 
 *********************************************/ 
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// struct for the tower of hanoi problem
typedef struct Tower
{
   int n;              // number of disks
   int * rods[3];      // rods representation
   int verbose;        // verbosity
} Tower;

Tower * Tower_ctor(int n);
void Tower_dtor(Tower * t);
void Tower_print(Tower * t);
void Tower_move_stack(Tower * t, int stack_size, int rod_src, int rod_dst, int rod_aux);
void Tower_move_one(Tower * t, int rod_src, int rod_dst);


void TowerOfHanoi( int n, int verbosity );


int main( int argc, char * argv[] )
{
   if( argc < 2 )
   {
      printf("Usage: %s N [v]\n", argv[0]);
      printf("  solves the tower of hanoi puzzle.\n");
      printf("  N is the number of disks in the puzzle.\n");
      printf("  include v for verbose mode.\n");
      return 0;
   }
   int verbose = 0;
   if(argc==3 && strcmp(argv[2],"v")==0)
   {
      verbose = 1;
   }
   TowerOfHanoi(atoi(argv[1]), verbose);
   return 0;
}

// main tower solving function
// 
// creates an instance of a tower of hanoi problem
void TowerOfHanoi( int n, int verbosity )
{
   //printf("Tower of Hanoi with %i disks\n", n);
   Tower * tower = Tower_ctor(n);
   tower->verbose = verbosity;
   if(tower->verbose) Tower_print(tower);
   Tower_move_stack(tower, n, 0, 2, 1);
   Tower_dtor(tower);
}

// tower of hanoi object constructor
//
// 
Tower * Tower_ctor(int n)
{
   Tower * t = malloc(sizeof(Tower));
   t->n = n;
   for(int i=0; i<3; i++)
   {
      t->rods[i] = malloc(sizeof(int)*(n+1));
   }
   for (int i=0; i<n; i++)
   {
      t->rods[0][i] = n-i;
      t->rods[1][i] = t->rods[2][i] = 0;
   }
   t->rods[0][n] = t->rods[1][n] = t->rods[2][n] = 0;
   return t;
}

// tower destructor
// frees the rods in the tower instance
void Tower_dtor(Tower * t)
{
   for(int i=0; i<3; i++)
   {
      free(t->rods[i]);
   }
}

// moves rings from the source rod to the destination rod via an auxillary rod
void Tower_move_stack(Tower * t, int stack_size, int rod_src, int rod_dst, int rod_aux)
{
   if(stack_size == 0)
   {
      return;
   }
   Tower_move_stack(t, stack_size-1, rod_src, rod_aux, rod_dst);
   Tower_move_one(t, rod_src, rod_dst);
   Tower_move_stack(t, stack_size-1, rod_aux, rod_dst, rod_src);
}

// moves a single ring from a source rod to a destination rod
void Tower_move_one(Tower * t, int rod_src, int rod_dst)
{
   if(t->rods[rod_src][0]==0)
   {
      perror("=Moving from empty rod.");
      exit(0);
   }
   int src_index = 0;
   int dst_index = 0;
   while(t->rods[rod_src][src_index+1]!=0) src_index++;
   while(t->rods[rod_dst][dst_index]!=0) dst_index++;

   printf("MOVE %c to %c\n", "ABC"[rod_src], "ABC"[rod_dst]);

   t->rods[rod_dst][dst_index] = t->rods[rod_src][src_index];
   t->rods[rod_src][src_index] = 0;

   if(t->verbose) Tower_print(t);
}

//prints the current placement of rings on the towers/rods
void Tower_print(Tower * t)
{
   if(t->verbose != 1) return;
   for(int i=t->n; i>=0; i--)
   {
      printf("\t");
      for(int j=0; j<3; j++)
      {
         t->rods[j][i]?printf("%i\t", t->rods[j][i]):printf("|\t");
      }
      printf("\n");
   }
}