/* Author: Nik Erickson
 * Date: 2/21/24
 * Version: 1.0
 * Description: this program is a restricted version of the coin collecting robot
 *              program. It is restricted in that the robot can only move in down
 *              and right directions. The robot is also restricted to spaces not
 *              occupied by a wall.
 * Example: ./w17 5 6 0 X 0 1 0 0 1 0 0 X 1 0 0 1 0 X 1 0 0 0 0 1 0 1 X X X 0 1 0

Should produce this output:
Board Inputed:
0       X       0       1       0       0
1       0       0       X       1       0
0       1       0       X       1       0
0       0       0       1       0       1
X       X       X       0       1       0

Coin Collecting Table:
0       0       0       0       0       0
1       1       1       0       0       0
1       2       2       0       0       0
1       2       2       3       3       4
0       0       0       3       4       4

The optimal path with this board is: 4

   */


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
   if(argc < 4)
   {
      printf("Usage: %s <rows> <cols> <board>\n", argv[0]);
      return 1;
   }
   int rows = atoi(argv[1]);
   int cols = atoi(argv[2]);
   int board[rows][cols];
   int i, j;
   int k = 3;
   for(i = 0; i < rows; i++)
   {
      for(j = 0; j < cols; j++)
      {
         if(*argv[k] == 'X')
         {
            board[i][j] = -1;
            k++;
            continue;
         }
         board[i][j] = atoi(argv[k]);
         k++;
      }
   }
   printf("Board Inputed:\n");
   for(i = 0; i < rows; i++)
   {
      for(j = 0; j < cols; j++)
      {
         if(board[i][j] == -1)
         {
            printf("X\t");
            continue;
         }
         printf("%d\t", board[i][j]);
      }
      printf("\n");
   }

   int coinTable[rows][cols];
   coinTable[0][0] = board[0][0];

   for(int j = 1; j < cols; j++)
   {
      if(board[0][j] == -1 || coinTable[0][j - 1] == -1)
      {
         coinTable[0][j] = -1;
      }
      else
      {
         coinTable[0][j] = coinTable[0][j - 1] + board[0][j];
      }
   }

   for(int i = 1; i < rows; i++)
   {
      if(board[i][0] == -1 || coinTable[i - 1][0] == -1)
      {
         coinTable[i][0] = -1;
      }
      else
      {
         coinTable[i][0] = coinTable[i - 1][0] + board[i][0];
      }
      for(int j = 1; j < cols; j++)
      {
         if(board[i][j] == -1)
         {
            coinTable[i][j] = -1;
         }
         else
         {
            if(coinTable[i - 1][j] > coinTable[i][j - 1])
            {
               coinTable[i][j] = coinTable[i - 1][j] + board[i][j];
            }
            else if(coinTable[i][j - 1] != -1)
            {
               coinTable[i][j] = coinTable[i][j - 1] + board[i][j];
            }
            else
            {
               coinTable[i][j] = -1;
            }
         }
      }
   }

   printf("\nCoin Collecting Table:\n");
   for(i = 0; i < rows; i++)
   {
      for(j = 0; j < cols; j++)
      {
         if(coinTable[i][j] == -1)
         {
            printf("0\t");
            continue;
         }
         printf("%d\t", coinTable[i][j]);
      }
      printf("\n");
   }


   printf("\nThe optimal path with this board is: %d\n", coinTable[rows - 1][cols - 1]);
   return 0;
}