/*********************************************
 * Id: eric7862
 * 
 * this program computes 2^n for any non negative integer
 * 
 * usage example:
 * w03 3
 * returns 8
 *********************************************/ 
#include <stdio.h>
#include <stdlib.h>

int Calc2nBad(int n);

int main(int argc, char *argv[])
{
   if(argc < 2)
   {
      printf("usage: %s n\nwhere n is an integer > 0\n", argv[0]);
      return 0;
   }
   int n = atoi(argv[1]);
   int n2 = Calc2nBad(n);
   printf("%d\n", n2);
   return 0;
}

// calculate 2^n recursively for any non-negative integer
int Calc2nBad(int n)
{
   if(n < 1) return 1;
   return Calc2nBad(n-1)+Calc2nBad(n-1);
}