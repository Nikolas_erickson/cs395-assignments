/*********************************************
 * Id: eric7862
 *
 * Tower of Hanoi solver - cyclic
 * This program solves tower of hanoi problems of any size
 * It takes the number of disks in the problem as an integer perameter
 * In the cyclic Hanoi puzzel there are the added restrictions:
 * A can only move to B
 * B can only move to C
 * C can only move to A
 *
 * usage example:
 * w07 2
 * returns
 * MOVE A to B
 * MOVE B to C
 * MOVE A to B
 * MOVE C to A
 * MOVE B to C
 * MOVE A to B
 * MOVE B to C
 *
 *********************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Tower
{
   int n;
   int *rods[3];
   int verbose;
} Tower;

Tower *Tower_ctor(int n, int verbosity);
void Tower_dtor(Tower *t);
void Tower_print(Tower *t);
void Tower_move_stack(Tower *t, int stack_size, int rod_src, int rod_dst, int rod_aux);
void Tower_move_stack_two(Tower *t, int stack_size, int rod_src, int rod_dst, int rod_aux);
void Tower_move_one(Tower *t, int rod_src, int rod_dst);

void TowerOfHanoi(int n, int verbosity);

int main(int argc, char *argv[])
{
   if (argc < 2)
   {
      printf("Usage: %s N\n", argv[0]);
      printf("  solves the tower of hanoi puzzle.\n");
      printf("  N is the number of disks in the puzzle.\n");
      return 0;
   }
   int verbose = 0;
   if (argc == 3 && strcmp(argv[2], "v") == 0)
   {
      verbose = 1;
   }
   TowerOfHanoi(atoi(argv[1]), verbose);
   return 0;
}

// main tower of hanoi solver function
void TowerOfHanoi(int n, int verbosity)
{
   Tower *tower = Tower_ctor(n, verbosity);
   //Tower_print(tower);
   Tower_move_stack_two(tower, n, 0, 2, 1);
   Tower_dtor(tower);
}

// tower of hanoi object constructor
Tower *Tower_ctor(int n, int verbosity)
{
   Tower *t = malloc(sizeof(Tower));
   t->n = n;
   for (int i = 0; i < 3; i++)
   {
      t->rods[i] = malloc(sizeof(int) * (n + 1));
   }
   for (int i = 0; i < n; i++)
   {
      t->rods[0][i] = n - i;
      t->rods[1][i] = t->rods[2][i] = 0;
   }
   t->rods[0][n] = t->rods[1][n] = t->rods[2][n] = 0;
   t->verbose = verbosity;
   return t;
}

//tower of hanoi object destructor
void Tower_dtor(Tower *t)
{
   for (int i = 0; i < 3; i++)
   {
      free(t->rods[i]);
   }
}

//helper function for moving a stack of rings
void Tower_move_stack(Tower *t, int stack_size, int rod_src, int rod_dst, int rod_aux)
{
   if (stack_size == 0)
   {
      return;
   }
   if (stack_size == 1)
   {
      Tower_move_one(t, rod_src, rod_dst);
      return;
   }
   Tower_move_stack_two(t, stack_size - 1, rod_src, rod_aux, rod_dst);
   Tower_move_one(t, rod_src, rod_dst);
   Tower_move_stack_two(t, stack_size - 1, rod_aux, rod_dst, rod_src);
}

//defines how a stack of rings moves
void Tower_move_stack_two(Tower *t, int stack_size, int rod_src, int rod_dst, int rod_aux)
{
   if (stack_size == 0)
   {
      return;
   }
   if (stack_size == 1)
   {
      Tower_move_one(t, rod_src, rod_aux);
      Tower_move_one(t, rod_aux, rod_dst);
      return;
   }
   Tower_move_stack_two(t, stack_size - 1, rod_src, rod_dst, rod_aux);
   Tower_move_one(t, rod_src, rod_aux);
   Tower_move_stack(t, stack_size - 1, rod_dst, rod_src, rod_aux);
   Tower_move_one(t, rod_aux, rod_dst);
   Tower_move_stack_two(t, stack_size - 1, rod_src, rod_dst, rod_aux);
}

//move a single ring from one tower to another
void Tower_move_one(Tower *t, int rod_src, int rod_dst)
{
   if (t->rods[rod_src][0] == 0)
   {
      perror("=Moving from empty rod.");
      exit(0);
   }
   int src_index = 0;
   int dst_index = 0;
   while (t->rods[rod_src][src_index + 1] != 0)
   {
      src_index++;
   }
   while (t->rods[rod_dst][dst_index] != 0)
   {
      dst_index++;
   }

   printf("Move %c to %c\n", "ABC"[rod_src], "ABC"[rod_dst]);
   t->rods[rod_dst][dst_index] = t->rods[rod_src][src_index];
   t->rods[rod_src][src_index] = 0;

   if (t->verbose == 1)
   {
      Tower_print(t);
   }
}

// print the current tower configuration
void Tower_print(Tower *t)
{
   for (int i = t->n; i >= 0; i--)
   {
      printf("\t");
      for (int j = 0; j < 3; j++)
      {
         t->rods[j][i] ? printf("%i\t", t->rods[j][i]) : printf("|\t");
      }
      printf("\n");
   }
}