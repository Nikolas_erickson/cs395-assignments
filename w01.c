/*********************************************
 * Id: eric7862
 * 
 * this program computes the minimum distance between
 * elements of an array
 * 
 * usage example:
 * w01 1 2 5 9
 * returns 1
 *********************************************/ 
#include <stdio.h>
#include <stdlib.h>


int MinDistance(int * A, int n);
int MaxInArray(int * A, int n);

int main(int argc, char *argv[])
{
   int n = argc-1;
   int * A = malloc(sizeof(int)*n);

   for(int i=0; i<n; i++)
   {
      A[i] = atoi(argv[i+1]);
   }

   int dmin = MinDistance(A, n);
   printf("%d\n", dmin);
   return 0;
}



//
// This function finds the minimum distance between elements
// of array A with length n
int MinDistance(int * A, int n)
{
   if(n<=1) return 0;
   // Input: Array of A[0,,n-1]) of integers given as command line arguements
   // Output: Minimum distance between two of its elements
   int dmin = abs(A[1]-A[0]);
   for( int i=0; i<n-1; i++)
   {
      for(int j=i+1; j<n; j++)
      {
         if (i!=j && abs(A[i]-A[j]) < dmin) dmin = abs(A[i]-A[j]);
      }
   }
   return dmin;
}