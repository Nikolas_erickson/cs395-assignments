/*********************************************
 * Id: eric7862
 *
 * Searches for a substring within a string
 *
 * usage example:
 * w08 abc abaabcbabcbabcbabbbabcbaabbcbabcabc
 * returns
 * Matches found at locations: 3 7 11 19 29 32
 *
 *********************************************/
#include <stdio.h>
#include <string.h>

int BruteForceStringMatch(char *T, char *P);

int main(int argc, char *argv[])
{
   if (argc > 3)
   {
      printf("your args are wrong\n");
      return 0;
   }
   printf("Matches found at locations: ");
   int matchIndex = 0;
   int offset = 0;
   char *s = argv[2];
   char *s2 = argv[1];
   int leadingSpace = 0;
   while (matchIndex != -1)
   {
      matchIndex = BruteForceStringMatch(s, s2);
      if (matchIndex != -1)
      {
         int x = matchIndex + offset;
         if(leadingSpace)
         {
            printf(" %d", x);
         }
         else
         {
            printf("%d", x);
            leadingSpace = 1;
         }
         offset += matchIndex + 1;
         s += matchIndex + 1;
      }
      // abc abaabcbabcbabcbabbbabcbaabbcbabcabc
      // 3 7 11 19 29 32
   }
   printf("\n");
   return 0;
}

/// @brief finds string P via primitive caveman methods
/// @param T haystack
/// @param P needle
/// @return first position of needle or -1
int BruteForceStringMatch(char *T, char *P)
{
   int n = strlen(T);
   int m = strlen(P);
   for (int i = 0; i <= n - m; i++)
   {
      int j = 0;
      while (j < m && P[j] == T[i + j])
      {
         j = j + 1;
      }
      if (j == m)
      {
         return i;
      }
   }
   return -1;
}