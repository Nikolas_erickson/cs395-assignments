

typedef struct Node
{
   void * data;
   struct Node * next;
} Node;
typedef struct List
{
   Node * head;
   Node * tail;
   int size;
} List;
List * listCon();
void listDCon(List * this);
void listAdd(List * this, void * data);
void listAddFront(List * this, void * data);
void * listRemove(List * this, void * data);
void * listRemoveFront(List * this);
void * listRemoveBack(List * this);
void listPrint(List * this);




List * listCon()
{
   List * this = malloc(sizeof(List));
   this->head = NULL;
   this->tail = NULL;
   this->size = 0;
   return this;
}

void listDCon(List * this)
{
   Node * current = this->head;
   while(current != NULL)
   {
      Node * next = current->next;
      free(current);
      current = next;
   }
   free(this);
}
void listAdd(List * this, void * data)
{
   Node * node = malloc(sizeof(Node));
   node->data = data;
   node->next = NULL;
   if(this->head == NULL)
   {
      this->head = node;
      this->tail = node;
   }
   else
   {
      this->tail->next = node;
      this->tail = node;
   }
   this->size++;
}
void listAddFront(List * this, void * data)
{
   Node * node = malloc(sizeof(Node));
   node->data = data;
   node->next = this->head;
   this->head = node;
   if(this->tail == NULL)
   {
      this->tail = node;
   }
   this->size++;
}
void * listRemove(List * this, void * data)
{
   Node * current = this->head;
   Node * previous = NULL;
   while(current != NULL)
   {
      if(current->data == data)
      {
         if(previous == NULL)
         {
            this->head = current->next;
         }
         else
         {
            previous->next = current->next;
         }
         if(current == this->tail)
         {
            this->tail = previous;
         }
         void * data = current->data;
         free(current);
         this->size--;
         return data;
      }
      previous = current;
      current = current->next;
   }
   return NULL;
}
void * listRemoveFront(List * this)
{
   if(this->head == NULL)
   {
      return NULL;
   }
   Node * node = this->head;
   this->head = this->head->next;
   void * data = node->data;
   free(node);
   this->size--;
   return data;
}
void * listRemoveBack(List * this)
{
   if(this->tail == NULL)
   {
      return NULL;
   }
   Node * current = this->head;
   Node * previous = NULL;
   while(current->next != NULL)
   {
      previous = current;
      current = current->next;
   }
   if(previous == NULL)
   {
      this->head = NULL;
      this->tail = NULL;
   }
   else
   {
      previous->next = NULL;
      this->tail = previous;
   }
   void * data = current->data;
   free(current);
   this->size--;
   return data;
}
void listPrint(List * this)
{
   Node * current = this->head;
   while(current != NULL)
   {
      current->data->print();
      current = current->next;
   }
   printf("\n");
}