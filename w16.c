/*********************************************
 * Id: eric7862
 *
 * uses the quick hull algorithm for finding a convex hull
 *
 * usage example:
 *
./w16 0 3 1 1 2 2 4 4 0 0 1 2 3 1 3 3
The points in Convex Hull are:
(0, 0) (0, 3) (4, 4) (3, 1) (0, 0)

./w16 0 0 0 4 -4 0 5 0 0 -6 1 0
The points in Convex Hull are:
(-4, 0) (0, 4) (5, 0) (0, -6) (-4, 0)

 *********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define debug(...) printf(__VA_ARGS__)

 // point2d stuff
typedef struct Point2D
{
   int x;
   int y;
} Point2D;

// quicksort stuff
void Quicksort(Point2D ** A, int l, int r, int n);
int HoarePartition(Point2D ** A, int l, int r);
void swap(Point2D ** i1, Point2D ** i2);

// list stuff
typedef struct Node
{
   void * data;
   struct Node * next;
   struct Node * prev;
} Node;
typedef struct List
{
   Node * head;
   Node * tail;
   int size;
} List;
List * listCon();
void listDCon(List * this);
void listAdd(List * this, void * data);
void listAddFront(List * this, void * data);
void * listRemove(List * this, void * data);
void * listRemoveFront(List * this);
void * listRemoveBack(List * this);
void listPrint(List * this);
void listAppendAndConsume(List * this, List * other);

//quick hull stuff

Point2D * pointCon(int x, int y);
List * quickHull(List * points);
List * quickHullRec(List * points, Point2D * p1, Point2D * p2, int side);
int determinant(Point2D * p1, Point2D * p2, Point2D * p3);
bool xlessThanPoint2D(void * p1, void * p2);
bool xgreaterThanPoint2D(void * p1, void * p2);
void printPoint2D(Point2D * p) { printf("(%d, %d) ", p->x, p->y); }



int main(int argc, char ** argv)
{
   if(argc < 7 || argc % 2 == 0)
   {
      printf("Usage: %s p1x p1y p2x p2y p3x p3y ...\n", argv[0]);
      printf("Uses quick-hull to find the convex hull of the set of points {p1, p2, p3}\n");
      return 0;
   }

   int n = (argc - 1) / 2;
   Point2D ** points = malloc(n * sizeof(Point2D *));


   for(int i = 0; i < n; i++)
   {
      points[i] = pointCon(atoi(argv[i * 2 + 1]), atoi(argv[i * 2 + 2]));
   }
   Quicksort(points, 0, n - 1, n);
   List * pointList = listCon();
   for(int i = 0; i < n; i++)
   {
      listAdd(pointList, points[i]);
   }

   quickHull(pointList);

   return 0;
}

Point2D * pointCon(int x, int y)
{
   Point2D * this = malloc(sizeof(Point2D));
   this->x = x;
   this->y = y;
   return this;
}


/// @brief returns the set of points that are on the convex hull of the set of points
/// @param points Array of Point2D
/// @param n number of points
List * quickHull(List * points)
{
   if(points->size < 3)
   {
      printf("Convex Hull not possible\n");
      return NULL;
   }

   Point2D * p1 = listRemoveFront(points);
   Point2D * p2 = listRemoveBack(points);

   List * hull = listCon();

   listAdd(hull, p1);
   List * leftList = quickHullRec(points, p1, p2, 1);
   if(leftList != NULL)
   {
      listAppendAndConsume(hull, leftList);
   }
   listAdd(hull, p2);
   List * rightList = quickHullRec(points, p2, p1, 1);
   if(leftList != NULL)
   {
      listAppendAndConsume(hull, rightList);
   }
   listAdd(hull, p1);

   printf("The points in Convex Hull are:\n");
   listPrint(hull); //can be removed

   return hull;
}

/// @brief Qiuck hull recursive helper function
/// @param points points to check for hull
/// @param p1 p1 of line
/// @param p2 p2 of line
/// @param side 
/// @return returns portion of the convex hull
List * quickHullRec(List * points, Point2D * p1, Point2D * p2, int side)
{
   if(points->size == 0)
   {
      return NULL;
   }
   List * hull = listCon();
   List * leftPoints = listCon();
   Point2D * Pmax = NULL;
   Node * currentNode = points->head;
   while(currentNode != NULL)
   {
      Point2D * p = currentNode->data;
      int d = determinant(p1, p2, p) * side;
      if(d > 0)
      {
         listAdd(leftPoints, p);
         if(Pmax == NULL || d > determinant(p1, p2, Pmax))
         {
            Pmax = p;
         }
      }
      currentNode = currentNode->next;
   }
   if(Pmax == NULL)
   {
      listDCon(leftPoints);
      return NULL;
   }
   else
   {
      List * leftList = quickHullRec(leftPoints, p1, Pmax, side);
      if(leftList != NULL)
      {
         listAppendAndConsume(hull, leftList);
      }
      listAdd(hull, Pmax);
      List * rightList = quickHullRec(leftPoints, Pmax, p2, side);
      if(rightList != NULL)
      {
         listAppendAndConsume(hull, rightList);
      }
      return hull;

   }
}




/// @brief Returns value proportional to area of triangle p1, p2, p3
/// I skipped the absolute value and halve part of the formula because I don't need it
/// @param p1 Point2D
/// @param p2 Point2D
/// @param p3 Point2D
/// @return value proportional to area of triangle p1, p2, p3
int determinant(Point2D * p1, Point2D * p2, Point2D * p3)
{
   return p1->x * (p2->y - p3->y) + p2->x * (p3->y - p1->y) + p3->x * (p1->y - p2->y);
}


/// @brief Returns true if p1 is less than p2 based on x, and then y
/// @param p1 Point2D
/// @param p2 Point2D
/// @return true if p1 is less than p2
bool xlessThanPoint2D(void * p1, void * p2)
{
   if(((Point2D *) p1)->x < ((Point2D *) p2)->x)
   {
      return true;
   }
   else if(((Point2D *) p1)->x == ((Point2D *) p2)->x)
   {
      return ((Point2D *) p1)->y < ((Point2D *) p2)->y;
   }
   else
   {
      return false;
   }
}

/// @brief returns true if p1 is greater than p2 based on x, and then y
/// @param p1 Point2D
/// @param p2 Point2D
/// @return true if p1 is greater than p2
bool xgreaterThanPoint2D(void * p1, void * p2)
{
   if(((Point2D *) p1)->x > ((Point2D *) p2)->x)
   {
      return true;
   }
   else if(((Point2D *) p1)->x == ((Point2D *) p2)->x)
   {
      return ((Point2D *) p1)->y > ((Point2D *) p2)->y;
   }
   else
   {
      return false;
   }
}





//quicksort stuff************************************************************************************************

/// @brief sorts an array via quicksort using Hoare's Partitioning
/// @param A pointer to array
/// @param l left index of subarray
/// @param r right index of subarray
/// @param n length of main array
void Quicksort(Point2D ** A, int l, int r, int n)
{
   if(l < r)
   {
      int s = HoarePartition(A, l, r); // s is a split position
      if(s > 0)
      {
         Quicksort(A, l, s - 1, n);
      }
      if(s < n - 1)
      {
         Quicksort(A, s + 1, r, n);
      }
   }
}

/// @brief partitions a subarray by Hoare's Algorithm, using the first element as a pivot
/// @param A pointer to main array
/// @param l left index of subarray
/// @param r right index of subarray
/// @return Partition of A[l..r], with the split position returned as this function’s value
int HoarePartition(Point2D ** A, int l, int r)
{
   Point2D * p = A[l];
   int i = l;
   int j = r + 1;
   while(1)
   {
      do
      {
         i++;
      }
      while(xlessThanPoint2D(A[i], p) && i < r);
      do
      {
         j--;
      }
      while(xgreaterThanPoint2D(A[j], p));
      if(i >= j)
      {
         swap(&A[l], &A[j]);
         return j;
      }
      swap(&A[i], &A[j]);
   }
}

/// @brief swaps elements of same type
/// @param i1 pointer to first item
/// @param i2 pointer to second item
void swap(Point2D ** i1, Point2D ** i2)
{
   // printf("swap %d, %d\n", *i1, *i2);
   Point2D * tmp = *i1;
   *i1 = *i2;
   *i2 = tmp;
}





//list stuff************************************************************************************************

List * listCon()
{
   List * this = malloc(sizeof(List));
   this->head = NULL;
   this->tail = NULL;
   this->size = 0;
   return this;
}
void listDCon(List * this)
{
   Node * current = this->head;
   while(current != NULL)
   {
      Node * next = current->next;
      free(current);
      current = next;
   }
   free(this);
}
void listAdd(List * this, void * data)
{
   Node * newNode = malloc(sizeof(Node));
   newNode->data = data;
   newNode->next = NULL;
   newNode->prev = this->tail;
   if(this->tail != NULL)
   {
      this->tail->next = newNode;
   }
   this->tail = newNode;
   if(this->head == NULL)
   {
      this->head = newNode;
   }
   this->size++;
}
void listAddFront(List * this, void * data)
{
   Node * newNode = malloc(sizeof(Node));
   newNode->data = data;
   newNode->next = this->head;
   newNode->prev = NULL;
   if(this->head != NULL)
   {
      this->head->prev = newNode;
   }
   this->head = newNode;
   if(this->tail == NULL)
   {
      this->tail = newNode;
   }
   this->size++;
}
void * listRemove(List * this, void * data)
{
   Node * current = this->head;
   while(current != NULL)
   {
      if(current->data == data)
      {
         if(current->prev != NULL)
         {
            current->prev->next = current->next;
         }
         else
         {
            this->head = current->next;
         }
         if(current->next != NULL)
         {
            current->next->prev = current->prev;
         }
         else
         {
            this->tail = current->prev;
         }
         this->size--;
         void * data = current->data;
         free(current);
         return data;
      }
      current = current->next;
   }
   return NULL;
}
void * listRemoveFront(List * this)
{
   if(this->head == NULL)
   {
      return NULL;
   }
   Node * current = this->head;
   this->head = current->next;
   if(this->head != NULL)
   {
      this->head->prev = NULL;
   }
   else
   {
      this->tail = NULL;
   }
   this->size--;
   void * data = current->data;
   free(current);
   return data;
}
void * listRemoveBack(List * this)
{
   if(this->tail == NULL)
   {
      return NULL;
   }
   Node * current = this->tail;
   this->tail = current->prev;
   if(this->tail != NULL)
   {
      this->tail->next = NULL;
   }
   else
   {
      this->head = NULL;
   }
   this->size--;
   void * data = current->data;
   free(current);
   return data;
}
void listAppendAndConsume(List * this, List * other)
{
   if(other->head == NULL)
   {
      return;
   }
   if(this->head == NULL)
   {
      this->head = other->head;
      this->tail = other->tail;
      this->size = other->size;
      return;
   }
   this->tail->next = other->head;
   other->head->prev = this->tail;
   this->tail = other->tail;
   this->size += other->size;
   free(other);
}
void listPrint(List * this)
{
   Node * current = this->head;
   while(current != NULL)
   {
      Point2D * p = (Point2D *) current->data;
      printPoint2D(p);
      current = current->next;
   }
   printf("\n");
}
