/*********************************************
 * Id: eric7862
 *
 * performs a horsepools algorithm for finding a substring
 *
 * usage example:
 *
./w14 abc abaabcbabcbabcbabbbabcbaabbcbabcabc
        !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /
3       3       3       3       3       3       3       3       3       3       3       3       3       3       3       3

0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?
3       3       3       3       3       3       3       3       3       3       3       3       3       3       3       3

@       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O
3       3       3       3       3       3       3       3       3       3       3       3       3       3       3       3

P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _
3       3       3       3       3       3       3       3       3       3       3       3       3       3       3       3

`       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o
3       2       1       3       3       3       3       3       3       3       3       3       3       3       3       3

p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~
3       3       3       3       3       3       3       3       3       3       3       3       3       3       3       3

abaabcbabcbabcbabbbabcbaabbcbabcabc
abc
  abc
   abc---found
      abc
       abc---found
          abc
           abc---found
              abc
               abc
                abc
                 abc
                   abc---found
                      abc
                        abc
                         abc
                            abc
                             abc---found
                                abc---found
Matches found at locations: 3 7 11 19 29 32
 *********************************************/
#include <stdio.h>
#include <stdlib.h>

typedef struct String
{
   char * content;
   int length;
} String;

String * stringCon(char * str);
void stringDcon(String * this);
int * makeShiftTable(String * needle);
void printTable(int * table);
int horspool(String * needle, String * haystack);
void printLine(int n, int m);

int main(int argc, char ** argv)
{
   if(argc < 3)
   {
      printf("Usage: %s str_needle str_haystack\n", argv[0]);
      printf("Uses Horspool's Algortithm to find a string within another string\n");
      return 0;
   }

   String * needle = stringCon(argv[1]);
   String * haystack = stringCon(argv[2]);
   horspool(needle, haystack);
   stringDcon(needle);
   stringDcon(haystack);
   return 0;
}

/// @brief constructor for String struct
/// @param str string to be stored
/// @return pointer to new String
String * stringCon(char * str)
{
   String * returnString;
   returnString = malloc(sizeof(String));
   //get string length
   int l = 0;
   while(str[l]) l++;
   returnString->length = l;
   //copy contents
   returnString->content = malloc(sizeof(char) * (l + 1));
   for(int i = 0; i <= l; i++)
   {
      returnString->content[i] = str[i];
   }
   return returnString;
}

/// @brief destructor for String struct
/// @param this pointer to String to be destructed
void stringDcon(String * this)
{
   free(this->content);
   free(this);
}

int horspool(String * needle, String * haystack)
{
   //make table
   int * table = makeShiftTable(needle);
   int * matches = malloc(sizeof(int) * haystack->length);
   int matchNum = 0;
   printf("%s\n", haystack->content);
   int needleLength = needle->length;
   int haystackLength = haystack->length;
   int i = needleLength - 1; //position of the pattern’s right end
   while(i <= haystackLength - 1)
   {
      int k = 0; //number of matched characters
      while((k <= needleLength - 1) && (needle->content[needleLength - 1 - k] == haystack->content[i - k]))
      {
         k = k + 1;
      }
      if(k == needleLength)
      {
         printf("%*s%s---found\n", i - needleLength + 1, "", needle->content);
         matches[matchNum] = i - needleLength + 1;
         matchNum++;
      }
      else
      {
         printf("%*s%s\n", i - needleLength + 1, "", needle->content);
      }
      i = i + table[(int) haystack->content[i]];
   }

   printf("Matches found at locations:");
   for(i = 0; i < matchNum; i++)
   {
      printf(" %d", matches[i]);
   }
   printf("\n");
   free(table);
   free(matches);
   return -1;
}

#define MAX_ALPHABET 128
#define TABLE_ROW_LENGTH 16
#define MIN_WRITEABLE 32
void printTable(int * table)
{
   for(int start = MIN_WRITEABLE; start < MAX_ALPHABET; start += TABLE_ROW_LENGTH)
   {
      for(int i = start; i < start + TABLE_ROW_LENGTH; i++)
      {
         printf("%c\t", i);
      }
      printf("\n");

      for(int i = start; i < start + TABLE_ROW_LENGTH; i++)
      {
         printf("%d\t", table[i]);
      }
      printf("\n\n");

   }
}

int * makeShiftTable(String * needle)
{
   int * table = malloc(sizeof(int) * 128);
   int strLen = needle->length;
   for(int i = 0; i < MAX_ALPHABET; i++)
   {
      table[i] = strLen;
   }
   for(int j = 0; j < strLen - 1; j++)
   {
      table[(int) needle->content[j]] = strLen - 1 - j;
   }
   printTable(table);
   return table;
}


/*


/// @brief find string needle in string haystack using Horspool's method
/// @param needle string to be found
/// @param haystack string to be searched in
/// @param offset starting offset for search, if negative search in reverse direction
/// @return location of first occurence
int horspool(String * needle, String * haystack, int offset)
{
   int reverseSearch = 0;
   if(offset < 0)
   {
      reverseSearch = 1;
      needle = stringRev(needle);
      haystack = stringRev(haystack);
      offset = -1 - offset;
   }
   //make table
   int * table = makeShiftTable(needle);
   int * matches = malloc(sizeof(int) * haystack->length);
   int matchNum = 0;
   int needleLength = needle->length;
   int haystackLength = haystack->length;
   int i = needleLength - 1 + offset; //position of the pattern’s right end
   while(i <= haystackLength - 1)
   {
      int k = 0; //number of matched characters
      while((k <= needleLength - 1) && (needle->content[needleLength - 1 - k] == haystack->content[i - k]))
      {
         k = k + 1;
      }
      if(k == needleLength)
      {
         if(reverseSearch)
         {
            //free reversed strings
            stringDcon(needle);
            stringDcon(haystack);
            return -1 - i + haystackLength;
         }
         else
         {
            return i - needleLength + 1;
         }
      }
      i = i + table[(int) haystack->content[i]];
   }
   free(table);
   free(matches);
   return -1;
}*/