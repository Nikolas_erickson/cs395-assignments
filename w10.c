/*********************************************
 * Id: eric7862
 *
 * performs a quicksort using Hoare's algorithm
 *
 * usage example:
 * w10 5628 6332 712 1585 7401 4430 6444 5153 5000 4940 3087 9973 54 523 2328 6907 7358 5769 6402 7964 4373 3735 207 8696 2343 6935 8770 8630 2246 5073 480
 * returns
 * 54 207 523 712 1585 2246 2328 2343 3087 3735 4373 4430 4803 4940 5000 5073 5153 5628 5769 6332 6402 6444 6907 6935 7358 7401 7964 8630 8696 8770 9973
 *
 *********************************************/
#include <stdio.h>
#include <stdlib.h>

void Quicksort(int *A, int l, int r, int n);
int HoarePartition(int *A, int l, int r);
void swap(int *i1, int *i2);
void printArray(int *A, int l, int r, int n);

int main(int argc, char *argv[])
{
   if (argc < 2)
   {
      printf("Usage: %s int1 int2 .. intN\n", argv[0]);
      printf("sorts an array of integers.\n");
      return 0;
   }

   int n = argc - 1;
   int A[n];
   for (int i = 0; i < n; i++)
   {
      A[i] = atoi(argv[i + 1]);
   }
   Quicksort(&A[0], 0, n - 1, n);
   printArray(A, 0, n-1, n);
   return 0;
}

/// @brief prints an array with a subarray delimited
/// @param A pointer to main array
/// @param l left index of subarray
/// @param r right index of subarray
/// @param n length of array
void printArray(int *A, int l, int r, int n)
{
   // Note: in printArray, print each value as an integer places followed by space:
   for (int i = 0; i < n; i++)
   {
      if(i == l)
      {
         printf("[");
      }
      printf("%d", A[i]);
      if (i == r)
      {
         printf("] ");
      }
      printf(" ");
   }
   printf("\n");
}

/// @brief sorts an array via quicksort using Hoare's Partitioning
/// @param A pointer to array
/// @param l left index of subarray
/// @param r right index of subarray
/// @param n length of main array
void Quicksort(int *A, int l, int r, int n)
{
   if(l < r)
   {
      int s = HoarePartition(A, l, r); // s is a split position
      if(s > 0)
      {
         Quicksort(A, l, s - 1, n);
      }
      if(s < n-1)
      {
         Quicksort(A, s + 1, r, n);
      }
      printArray(A, l, r, n);
   }
}

/// @brief partitions a subarray by Hoare's Algorithm, using the first element as a pivot
/// @param A pointer to main array
/// @param l left index of subarray
/// @param r right index of subarray
/// @return Partition of A[l..r], with the split position returned as this function’s value
int HoarePartition(int *A, int l, int r)
{
   // printArray(A, l, r, 6);
   int p = A[l];
   int i = l;
   int j = r + 1;
   while(1)
   {
      do
      {
         i++;
      } while(A[i] < p && i < r);
      do
      {
         j--;
      } while(A[j] > p);
      if (i >= j)
      {
         swap(&A[l], &A[j]);
         // printArray(A, l, r, 6);
         // printf("pivot A[%i]=%i\n", j, A[j]);
         return j;
      }
      swap(&A[i], &A[j]);
   }
}

/// @brief swaps elements of same type
/// @param i1 pointer to first item
/// @param i2 pointer to second item
void swap(int *i1, int *i2)
{
   // printf("swap %d, %d\n", *i1, *i2);
   int tmp = *i1;
   *i1 = *i2;
   *i2 = tmp;
}