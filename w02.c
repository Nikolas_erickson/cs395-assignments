/*********************************************
 * Id: eric7862
 * 
 * this program computes the minimum distance between
 * elements of an array
 * 
 * I use a quicksort and then find the min distance
 * which should be an O(nlogn) in the average case rather 
 * than the O(n²) the previous implementation was
 * 
 * usage example:
 * w01 1 2 5 9
 * returns 1
 *********************************************/ 
#include <stdio.h>
#include <stdlib.h>

void Quicksort(int * A, int n);
int MinDistance(int * A, int n);
int MaxInArray(int * A, int n);
void PrintArray(int * A, int n);

int main(int argc, char *argv[])
{
   int n = argc-1;
   int * A = malloc(sizeof(int)*n);

   for(int i=0; i<n; i++)
   {
      A[i] = atoi(argv[i+1]);
   }

   int dmin = MinDistance(A, n);
   printf("%d\n", dmin);
   return 0;
}

//This is the main quicksort function
void Quicksort(int * A, int n)
{
   if (n <= 1) return;
   //pivot is rightmost element
   int p = A[n-1];
   //start by selecting elements at each end of the array and work inward
   int fromLeft = 0;
   int fromRight = n-2;
   //until the pointers cross
   while(fromLeft < fromRight)
   {
      //find elements to switch with eachother based on < or > than pivot element
      while(A[fromLeft]<=p && fromLeft<n-2) fromLeft++;
      while(A[fromRight]>p && fromRight>0) fromRight--;
      if(fromLeft<fromRight)
      {
         //swap elements in place
         A[fromRight] += A[fromLeft];
         A[fromLeft] = A[fromRight] - A[fromLeft];
         A[fromRight] = A[fromRight] - A[fromLeft];
      }
   }
   if(p<A[fromLeft])
   {
      A[n-1] = A[fromLeft];
      A[fromLeft] = p;
   }
   Quicksort(A,fromRight+1);
   Quicksort(A+fromLeft+1,n-fromLeft-1);
}

// the min distance function from the previous assignment
int MinDistance(int * A, int n)
{
   if(n<=1) return 0;
   Quicksort(A,n);
   // Input: Array of A[0,,n-1]) of integers given as command line arguements
   // Output: Minimum distance between two of its elements
   int dmin = abs(A[1]-A[0]);
   for (int i =2; i < n-1; i++)
   {
      if (abs(A[i]-A[i-1])<dmin) dmin = abs(A[i]-A[i-1]);
   }
   return dmin;
}

// a function for printing out an array
void PrintArray(int * A, int n)
{
   for(int i =0; i<n; i++) printf("%d ", A[i]);
   printf("\n");
}