/* Author: Nik Erickson
 * Date: 02/21/24
 * Version: 1.0
 *
 * Description: this program does an exhaustive search for the largest clique in a graph
 *
 * Usage: ./w18 <num_vertices> upper triangle of graph
 *
 * Example: ./w18 6  1 0 0 1 1  1 1 0 0  1 1 1  0 1  1
Represents the graph
  1 0 0 1 1
    1 1 0 0
      1 1 1
        0 1
          1

Your output should be like this:
./clique 7 1 1 0 1 1 0   1 1 0 1 0     1 0 0 1     1 1 1     0 1      0
0 1 1 0 1 1 0
1 0 1 1 0 1 0
1 1 0 1 0 0 1
0 1 1 0 1 1 1
1 0 0 1 0 0 1
1 1 0 1 0 0 0
0 0 1 1 1 0 0
No clique found of size 7
No clique found of size 6
No clique found of size 5
No clique found of size 4
Clique found of size 3
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>






 // find cliques stuff
void findCliques(int * graph, __uint32_t numVertices);
bool isClique(int * graph, int numVertices, __uint32_t vertexList);
int countVertices(__uint32_t vertexList);
int power(int base, int exp);

int main(int argc, char * argv[])
{
   if(argc < 3)
   {
      printf("Usage: %s <num_vertices> <upper triangle of graph>\n", argv[0]);
      return 1;
   }
   __uint32_t num_vertices = atoi(argv[1]);
   int graph[num_vertices][num_vertices];
   int i, j;
   int k = 2;
   for(i = 0; i < num_vertices - 1; i++)
   {
      for(j = i + 1; j < num_vertices; j++)
      {
         graph[i][j] = atoi(argv[k]);
         k++;
      }
   }
   for(i = 0; i < num_vertices; i++)
   {
      for(j = 0; j < num_vertices; j++)
      {
         if(i > j)
         {
            graph[i][j] = graph[j][i];
         }
         else if(i == j)
         {
            graph[i][j] = 0;
         }
      }
   }
   for(i = 0; i < num_vertices; i++)
   {
      for(j = 0; j < num_vertices; j++)
      {
         printf("%d ", graph[i][j]);
      }
      printf("\n");
   }
   findCliques((int *) &graph, num_vertices);
   return 0;
}

void findCliques(int * graph, __uint32_t numVertices)
{
   int clique_size = numVertices;
   int vertexcombinations = power(2, numVertices);
   while(clique_size > 0)
   {
      for(__uint32_t vertexList = 0; vertexList < vertexcombinations; vertexList++)
      {
         if(countVertices(vertexList) != clique_size)
         {
            continue;
         }
         if(isClique(graph, numVertices, vertexList) == 1)
         {
            printf("Clique found of size %d\n", clique_size);
            return;
         }
      }
      printf("No clique found of size %d\n", clique_size);
      clique_size--;
   }
}

int countVertices(__uint32_t vertexList)
{
   int count = 0;
   while(vertexList > 0)
   {
      if(vertexList % 2 == 1)
      {
         count++;
      }
      vertexList = vertexList / 2;
   }
   return count;
}

/// @brief power function returns base to the power of exp
/// @param base integer base
/// @param exp integer exponent
/// @return base to the power of exp
int power(int base, int exp)
{
   int result = 1;
   for(int i = 0; i < exp; i++)
   {
      result *= base;
   }
   return result;
}

/// @brief checks if the vertexList is a clique of graph 
/// @param graph 2d array representing the graph adjacency matrix
/// @param numVertices number of vertices in the graph
/// @param vertexList list of vertices to check, in the form of a bitfield
/// @return boolean 1 if clique, 0 if not
bool isClique(int * graph, int numVertices, __uint32_t vertexList)
{
   //create array of vertices
   bool vertices[numVertices];
   for(int i = 0; i < numVertices; i++)
   {
      vertices[i] = (vertexList >> i) & 1;
   }
   //check if each vertex is connected to every other vertex
   for(int i = 0; i < numVertices - 1; i++)
   {
      if(vertices[i])
      {
         for(int j = i + 1; j < numVertices; j++)
         {
            if(vertices[j])
            {
               if(graph[i * numVertices + j] == 0)
               {
                  return false;
               }
            }
         }
      }
   }
   return true;
}
