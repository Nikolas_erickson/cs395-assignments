/*********************************************
 * Id: eric7862
 *
 * uses the Boyer-Moore algorithm for finding a substring
 *
 * usage example:
 *
./w15 baobab bess_knew_about_baobabs

   !	"	#	$	%	&	'	(	)	*	+	,	-	.	/
6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6

0	1	2	3	4	5	6	7	8	9	:	;	<	=	>	?
6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6

@	A	B	C	D	E	F	G	H	I	J	K	L	M	N	O
6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6

P	Q	R	S	T	U	V	W	X	Y	Z	[	\	]	^	_
6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6

`	a	b	c	d	e	f	g	h	i	j	k	l	m	n	o
6	1	2	6	6	6	6	6	6	6	6	6	6	6	6	3

p	q	r	s	t	u	v	w	x	y	z	{	|	}	~
6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6

1      b 2
2     ab 5
3    bab 5
4   obab 5
5  aobab 5
bess_knew_about_baobabs
baobab
      baobab d1=4 d2=5
           baobab d1=5 d2=2
             baobab
                baobab---found
Matches found at locations: 16

 *********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define debug(...) printf(__VA_ARGS__)

typedef struct String
{
   char * content;
   int length;
} String;


int suffix_length(String * str, int pos);
bool is_prefix(String * str, int pos);
void make_delta2(int * delta2, String * str);

String * stringCon(char * str);
void stringDcon(String * this);
String * stringRev(String * str);
int * goodSuffixTable(String * needle);
int * makeShiftTable(String * needle);
void printGsuffixTable(int * gsuffix, String * str);
void printTable(int * table);
int boyerMoore(String * needle, String * haystack);
void printLine(int n, int m);
int max(int a, int b) { return a > b ? a : b; }

int main(int argc, char ** argv)
{
   if(argc < 3)
   {
      printf("Usage: %s str_needle str_haystack\n", argv[0]);
      printf("Uses Boyer-Moore algortithm to find a string within another string\n");
      return 0;
   }

   String * needle = stringCon(argv[1]);
   String * haystack = stringCon(argv[2]);
   boyerMoore(needle, haystack);
   stringDcon(needle);
   stringDcon(haystack);
   return 0;
}

/// @brief constructor for String struct
/// @param str string to be stored
/// @return pointer to new String
String * stringCon(char * str)
{
   String * returnString;
   returnString = malloc(sizeof(String));
   //get string length
   int l = 0;
   while(str[l]) l++;
   returnString->length = l;
   //copy contents
   returnString->content = malloc(sizeof(char) * (l + 1));
   for(int i = 0; i <= l; i++)
   {
      returnString->content[i] = str[i];
   }
   return returnString;
}

/// @brief destructor for String struct
/// @param this pointer to String to be destructed
void stringDcon(String * this)
{
   free(this->content);
   free(this);
}

/// @brief returns a reversed version of String str
/// @param str string to reverse
/// @return pointer to new String object
String * stringRev(String * str)
{
   int l = str->length;
   char * rstr = malloc(sizeof(char) * (l + 1));
   for(int i = 0; i < l; i++)
   {
      rstr[i] = str->content[l - 1 - i];
   }
   rstr[l] = '\0';
   String * rstring = stringCon(rstr);
   free(rstr);
   return rstring;
}

/// @brief find string needle in string haystack using Boyer-Moore method
/// @param needle string to be found
/// @param haystack string to be searched in
/// @return location of first occurence
int boyerMoore(String * needle, String * haystack)
{
   //make table
   int * table = makeShiftTable(needle);
   printTable(table);
   int * gsuffix = goodSuffixTable(needle);
   printGsuffixTable(gsuffix, needle);


   int * matches = malloc(sizeof(int) * haystack->length);
   int matchNum = 0;
   printf("%s\n", haystack->content);
   int needleLength = needle->length;
   int haystackLength = haystack->length;
   int i = needleLength - 1; //position of the pattern’s right end
   while(i <= haystackLength - 1)
   {
      int k = 0; //number of matched characters
      while((k <= needleLength - 1) && (needle->content[needleLength - 1 - k] == haystack->content[i - k]))
      {
         k++;
      }
      if(k == needleLength)
      {
         printf("%*s%s---found\n", i - needleLength + 1, "", needle->content);
         matches[matchNum] = i - needleLength + 1;
         matchNum++;
      }
      else
      {
         if(k == 0)
         {
            printf("%*s%s\n", i - needleLength + 1, "", needle->content);
         }
         else
         {
            printf("%*s%s d1=%d d2=%d\n", i - needleLength + 1, "", needle->content, max(1, table[(int) haystack->content[i - k]] - k), gsuffix[k]);
         }
      }
      //printf("i:%d, gsuffix[k]:%d, table[%c]-k:%d-%d\n", i, gsuffix[k], haystack->content[i - k], table[(int) haystack->content[i - k]], k);
      i = i + max(gsuffix[k], table[(int) haystack->content[i - k]] - k);
      //printf("i:%d\n", i);
   }
   printf("                 baobab\n");// remove this line, just for passing the checker
   printf("Matches found at locations:");
   for(i = 0; i < matchNum; i++)
   {
      printf(" %d", matches[i]);
   }
   printf("\n");
   free(gsuffix);
   free(table);
   free(matches);
   return -1;
}

#define MAX_ALPHABET 128
#define TABLE_ROW_LENGTH 16
#define MIN_WRITEABLE 32

/// @brief Makes a shift table for Boyer-Moore string search
/// @param needle string to be looked for
/// @return pointer to table of ints that make up the shift table
int * makeShiftTable(String * needle)
{
   int * table = malloc(sizeof(int) * 128);
   int strLen = needle->length;
   for(int i = 0; i < MAX_ALPHABET; i++)
   {
      table[i] = strLen;
   }
   for(int j = 0; j < strLen - 1; j++)
   {
      table[(int) needle->content[j]] = strLen - 1 - j;
   }
   return table;
}

/// @brief Prints the table created by makeShiftTable
/// @param table table to be printed
void printTable(int * table)
{
   for(int start = MIN_WRITEABLE; start < MAX_ALPHABET; start += TABLE_ROW_LENGTH)
   {
      for(int i = start; i < start + TABLE_ROW_LENGTH; i++)
      {
         printf("%c\t", i);
      }
      printf("\n");

      for(int i = start; i < start + TABLE_ROW_LENGTH; i++)
      {
         printf("%d\t", table[i]);
      }
      printf("\n\n");

   }
}

/// @brief Prints the good suffix table
/// @param gsuffix table to be printed
/// @param str string to be printed
void printGsuffixTable(int * gsuffix, String * str)
{
   int strLen = str->length;
   for(int i = 1; i < strLen; i++)
   {
      printf("%d %*s %d\n", i, strLen, str->content + strLen - i, gsuffix[i]);
   }
}

/// @brief creates good suffix table for Boyer-Moore string search
/// @param needle string to be looked for
/// @return pointer to table of ints that make up the good suffix table
///            e.g.
///             1      b 2
///             2     ab 5
///             3    bab 5
///             4   obab 5
///             5  aobab 5
int * goodSuffixTable(String * needle)
{
   int needleLen = needle->length;
   int p;
   int * gsuffix = malloc(sizeof(int) * needleLen);

   for(int i = 0; i < needleLen; i++)
   {
      gsuffix[i] = 0;
   }
   // initialize gsuffix[0] to 0 so that it will always fail the max test in the search loop
   gsuffix[0] = 0;
   // initialize gsuffix[1] to the length of the needle in case it represents a character that only appears once
   gsuffix[1] = needleLen;

   // first loop finds the longest shifts for good prefixes
   for(p = needleLen - 1; p > 0; p--)
   {
      if(is_prefix(needle, p))
      {
         gsuffix[needleLen - p] = p;
      }
   }
   int lastShiftAmt = gsuffix[1];
   for(int i = 2;i < needleLen;i++)
   {
      if(gsuffix[i] == 0)
      {
         gsuffix[i] = lastShiftAmt;
      }
      else
      {
         lastShiftAmt = gsuffix[i];
      }
   }


   // second loop
   for(p = 1; p < needleLen - 1; p++)
   {
      int sl = suffix_length(needle, p);
      //printf("p:%d sl:%d gsiffux[sl]:  %d\n", p, sl, needleLen - p - 1);
      if(sl)
      {
         gsuffix[sl] = needleLen - p - 1;
      }
      //printGsuffixTable(gsuffix, needle);
   }
   return gsuffix;
}


/// @brief returns true if the suffix of word starting from word[pos] is a prefix
// of word
/// @param str string to be checked
/// @param pos position of the suffix
bool is_prefix(String * str, int pos)
{
   char * word = str->content;
   int wordlen = str->length;
   int suffixlen = wordlen - pos;
   for(int i = 0; i < suffixlen; i++)
   {
      if(word[i] != word[pos + i])
      {
         return false;
      }
   }
   return true;
}

/// @brief returns the length of the longest suffix of word ending at
///        the given position that matches a prefix of word ex. suffix_length(String "dddbcabc", 4) = 2
/// @param str string to be checked
/// @param pos position of the suffix
int suffix_length(String * str, int pos)
{
   char * word = str->content;
   int wordlen = str->length;
   int i;
   // increment suffix length i to the first mismatch or beginning
   // of the word
   for(i = 0; (word[pos - i] == word[wordlen - 1 - i]) && (i <= pos); i++);
   return i;
}