/*********************************************
 * Id: eric7862
 * 
 * Gaussian Elimination
 * Uses Gaussian elimination to solve a system of linear equations.
 * 
 * usage example:
 * w04 3 1 2 3 4 5 6 7 8 9 10 11 12
 * returns
 * 1.00    2.00    3.00    | 4.00
 * 0.00    -4.00   -8.00   | -12.00
 * 0.00    0.00    0.00    | 0.00
 *********************************************/ 
#include <stdio.h>
#include <stdlib.h>


void GE( int n, float A[n][n+1] );
void printMatrix( int n, float A[n][n+1] );

int main( int argc, char * argv[] )
{
   if(argc<2)
   {
      printf("Usage: %s N E11 E12 .. E1M E21 E22 .. E2M ..\n", argv[0]);
      printf("  where N is the number of equations in the system\n");
      printf("  and M = N + 1\n");
      printf("\n");
      printf("Example: %s 3 1 2 3 4 5 6 7 8 9 10 11 12\n", argv[0]);
      printf(" Produces the matrix:\n");
      printf("  1  2  3  4\n");
      printf("  5  6  7  8\n");
      printf("  9 10 11 12\n");
      return 0;
   }
   
   int n = atoi(argv[1]);
   float Arr[n][n+1];
   for(int i = 0; i<n; i++)
   {
      for(int j = 0; j<=n; j++)
      {
         Arr[i][j] = atof(argv[i*(n+1)+j+2]);
      }
}
   GE(n, Arr);
}

// guassian elimination function
void GE( int n, float A[n][n+1] ) {
   printMatrix(n, A);
   for(int i = 0; i < n-1; i++)
   {
      for(int j = i+1; j < n; j++)
      {
         float tempji = A[j][i];
         for( int k = i; k <= n; k++)
         {
            A[j][k] = A[j][k] - (A[i][k] * tempji / A[i][i]);
         }
      }
      printMatrix(n, A);
   }
}

//prints the matrix
void printMatrix( int n, float A[n][n+1] )
{
   //Note: in printMatrix, print each value to 2 decimal places followed by space:
   for(int i=0; i<n; i++)
   {
      int j = 0;
      for( ; j<n; j++)
      {
         printf("%.2f ", A[i][j]);
      }
      printf("%.2f \n", A[i][j]);
   }
   printf("\n");
}

//prints the matrix in a prettier format
void printMatrixPretty( int n, float A[n][n+1] )
{
   //Note: in printMatrix, print each value to 2 decimal places followed by space:
   for(int i=0; i<n; i++)
   {
      int j = 0;
      for( ; j<n; j++)
      {
         printf("%.2f\t", A[i][j]);
      }
      printf("| %.2f\n", A[i][j]);
   }
   printf("\n");
}
